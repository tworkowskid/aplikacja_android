package example.com.sailing;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainLoginFragment extends Fragment {

    public static final String TAG = "MainLoginFragment";

    @Bind(R.id.button_register)
    protected Button register;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_main_login, container, false);

        ButterKnife.bind(this, result);

        return result;
    }

    @OnClick(R.id.button_register)
    protected void onRegisterClick(View view){
        Log.d("setup", "frag");
        ((LoginActivity)getActivity()).setupRegisterFragment();
    }

}
