package example.com.sailing;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupMainFragment();
    }

    protected void setupMainFragment(){
        addFragment(new MainLoginFragment(), MainLoginFragment.TAG);
    }

    protected void setupRegisterFragment(){
        addFragment(new RegisterFragment(), RegisterFragment.TAG);
    }

    protected void addFragment(Fragment fragment, String tag){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.addToBackStack(tag);
        ft.replace(R.id.fragment_container, fragment, tag);

        ft.commit();
    }

}
