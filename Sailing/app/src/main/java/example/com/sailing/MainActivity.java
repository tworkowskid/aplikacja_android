package example.com.sailing;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupMainFragment();
    }

    protected void setupMainFragment(){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.addToBackStack(MainLoginFragment.TAG);
        ft.add(R.id.fragment_container, new MainLoginFragment(), MainLoginFragment.TAG);

        ft.commit();
    }

}
